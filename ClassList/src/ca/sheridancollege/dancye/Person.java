package ca.sheridancollege.dancye;

public class Person {

	private String name;
	private String role;
	public Person(String givenRole, String givenName) 
	{
		name = givenName;
		role = givenRole;
	}
	
	public String toString()
	{
		return role + ": " + name ;
		
	}
}
